%{
#include <stdio.h>
#define YYSTYPE char*
int yyparse();
int yylex();
int yyerror(char *s);

#include "shell.h"

extern char * yytext;
%}

//Symboles terminaux qui seront fournis par yylex()
%token ENTIER
%token STRING
%token PATH
%token CMDPWD
%token CMDPATH
%token DELPATH
%token SHOWPATH
%token ADDPATH
%token ECHO1
%token MESSAGE
%%

commande: 
    | commande ECHO1 MESSAGE {echo1(yytext);}
	| commande DELPATH {deletePath();}
	| commande SHOWPATH {showPath();}
	| commande CMDPWD {displayPwd();}
	| commande ADDPATH PATH {addPath(yytext);}
;

%%

int yyerror(char *s) {
    printf("yyerror : %s\n",s);
    return 0;
}

int main(void) {
    yyparse();
    return 0;
}
