

#cette ligne sert à faire l'analyse syntaxique des commandes
bison grammar.y --defines=main.h -o main.c
#Compiler le fichier main.c en donnant le nom main.o
gcc -c main.c
#cette ligne permets de generer l'analyseur lexical, et le lecxique a ete defini dans le fichier syntax.flex
flex -o syntax.c syntax.flex
#Compiler le fichier syntax.c en donnant le nom syntax.o
gcc -c syntax.c
#Compiler le fichier shell.c en donnant le nom shell.o
gcc -c shell.c
#Creer un seul fichier executable de nom main pour trois fichier compilé a la fois 
gcc -o main shell.o syntax.o main.o



