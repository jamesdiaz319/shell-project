#include <stdio.h>
#include "string.h"
#include <libc.h>

void displayPrompt();

char GlobalPath[256] ="";

void showPath()
{
	printf("PATH=%s\n", GlobalPath);
	displayPrompt();
}

void deletePath()
{
	GlobalPath[0]='\0';
	displayPrompt();
}

void addPath(char *path)
{
	strncat(GlobalPath, ":", 100);
	strncat(GlobalPath, path, 100);
	printf("PATH=%s\n",GlobalPath);
	displayPrompt();
}

void affect(char *valeur)
{
	//char *nom;
	//char *variable1;
	//nom=variable;
	//variable1=valeur;
	//printf("MESSAGE1=%s\n",variable);
	printf("MESSAGE2=%s\n",valeur);
	//printf("ASCII value = %s, Character = %s\n",nom,valeur);
	displayPrompt();
}


void displayPrompt()
{
	char buffer[256];
	getwd(buffer);
	printf("%s >", buffer);
}

void echo(char *s)
{
	printf("%s\n", s);
	displayPrompt();
}

void displayPwd()
{
	char buffer[256];
	getwd(buffer);
	printf("%s\n", buffer);
	displayPrompt();
}
