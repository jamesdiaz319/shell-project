%{
#include <stdio.h>
#define YYSTYPE char*
int yyparse();
int yylex();
int yyerror(char *s);

#include "shell.h"

extern char * yytext;
%}

//Symboles terminaux qui seront fournis par yylex()
%token ENTIER
%token PATH
%token CMDPWD
%token CMDPATH
%token DELPATH
%token SHOWPATH
%token ADDPATH
%token ECHO1
%token STRING
%token EXEC
%token AFFECT
%%

commande: 
	| commande DELPATH {deletePath();}
	| commande SHOWPATH {showPath();}
	| commande CMDPWD {displayPwd();}
	| commande ADDPATH PATH {addPath(yytext);}
	| commande ECHO1 PATH {echo(yytext);}
	| commande ECHO1 STRING {echo(yytext);}
	| commande EXEC {echo(yytext);}
	| commande PATH {printf("KOFFI%d\n",PATH);} AFFECT STRING {affect(yytext);}
	| commande extcmd 
;

extcmd: /* vide */
	| PATH {printf("%s\n", yytext);} extcmd
;

%%

int yyerror(char *s) {
    printf("yyerror : %s\n",s);
    return 0;
}

int main(void) {
    yyparse();
    return 0;
}
