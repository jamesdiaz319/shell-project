%{
#include "main.h"
#define YYSTYPE char*
%}

%option noyywrap

blanks      [ \t\n]+
entier      [0-9]+
path        [a-zA-Z0-9\/]+
message     [a-zA-Z0-9]+
cmdpwd	    pwd
cmdaddpath  addpath
cmddelpath  delpath
cmdshowpath showpath
cmdecho     echo
%%

{blanks}        { /* ignore */ }

{entier}    	{return(ENTIER); }
{cmdpwd}    	{return(CMDPWD);}
{cmddelpath}	{return(DELPATH);}
{cmdshowpath} 	{return(SHOWPATH);}
{cmdaddpath}    {return(ADDPATH);}
{cmdecho}       {return(ECHO1);}
{message}		{return(MESSAGE);}
{path}		    {return(PATH);}
